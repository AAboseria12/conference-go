from .keys import PEXELS_API_KEY
import requests


def get_photo(city, state):
    pexels_api_key = PEXELS_API_KEY
    headers = {
        "Authorization": pexels_api_key,
    }
    url = "https://api.pexels.com/v1/search"
    query_params = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    response = requests.get(url, headers=headers, params=query_params)
    response_json = response.json()
    photos = response_json["photos"]
    picture_url = photos[0].get("url")

    return {"picture_url": picture_url}
