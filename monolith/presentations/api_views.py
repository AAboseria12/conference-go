from django.http import JsonResponse
from .models import Presentation
from common.json import ModelEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


def api_list_presentations(request, conference_id):
    presentation = Presentation.objects.filter(conference=conference_id)
    return JsonResponse(
        {"presentation": presentation},
        encoder=PresentationListEncoder,
    )


def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
